#ifndef __THREAD_H__
#define __THREAD_H__

//==========Thread library==========

int thread_create(void *(*start_routine)(void*), void *arg);
void thread_join();


//==========Spinlock==========

struct spinlock_t {
  uint locked;       // Is the lock held?
};

void spinlock_init(struct spinlock_t *lock);
void spinlock_acquire(struct spinlock_t *lock);
void spinlock_release(struct spinlock_t *lock);

//==========Array-based queue lock==========

uint n;

struct abql_lock_t {
  volatile struct {
    volatile int x;
  } has_lock[1024];
  volatile uint queueLast;
  uint holderPlace;
};

void abql_lock_init(struct abql_lock_t *lock, uint nprocs);
void abql_lock_acquire(struct abql_lock_t *lock);
void abql_lock_release(struct abql_lock_t *lock);

//==========Sequential lock==========

static void inline smp_rmb()
{
	asm volatile("lfence":::"memory");
}

static void inline smp_wmb()
{
	asm volatile("sfence":::"memory");
}

static void inline smp_mb()
{
	asm volatile("mfence":::"memory");
}

struct seqlock_t {
    uint seq;
    struct spinlock_t lock;
};

static inline void seqlock_init(struct seqlock_t *lock)
{
    lock->seq = 0;
    spinlock_init(&lock->lock);
}

static inline void seqlock_write_acquire(struct seqlock_t *lock)
{
    spinlock_acquire(&lock->lock);
    ++lock->seq;
    smp_wmb();
}

static inline void seqlock_write_release(struct seqlock_t *lock)
{
    smp_wmb();
    ++lock->seq;
    spinlock_release(&lock->lock);
}

#define ACCESS_ONCE(x) (*(volatile typeof(x)*)&(x))
static inline uint seqlock_read_begin(struct seqlock_t *lock)
{
    uint ret;
    
repeat:
	ret = ACCESS_ONCE(lock->seq);
	if (ret & 1) {
		goto repeat;
	}
	smp_rmb();

	return ret;
}

static inline int seqlock_read_retry(struct seqlock_t *lock, uint start)
{
    smp_rmb();
    return (lock->seq != start);
}
#endif
