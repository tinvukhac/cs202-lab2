#include "types.h"
#include "stat.h"
#include "fcntl.h"
#include "user.h"
#include "x86.h"
#include "mmu.h"
#include "thread.h"

//==========Thread library==========

int
thread_create(void *(*start_routine)(void*), void *arg)
{
  void *stack = malloc(4096);  
  int rc = clone(stack, 4096);
  if (rc == 0)
  {
    (*start_routine)(arg);
    free(stack);
    exit();
  }
  else
    return rc;
}

void
thread_join()
{
  wait();
}

//==========Spinlock==========

static inline unsigned int
test_and_set(volatile unsigned int *addr)
{
  unsigned int result;
  unsigned int new = 1;
  
  // x86 atomic exchange.
  asm volatile("lock; xchgl %0, %1" :
               "+m" (*addr), "=a" (result) :
               "1" (new) :
               "cc");
  return result;
}

void
spinlock_init(struct spinlock_t *lock)
{
  lock->locked = 0;
}

void
spinlock_acquire(struct spinlock_t *lock)
{ 
  //while(xchg(&lock->locked, 1) != 0);
  while(test_and_set(&lock->locked) == 1);
}

void
spinlock_release(struct spinlock_t *lock)
{
  //xchg(&lock->locked, 0);
  lock->locked = 0;
}

//==========Array-based queue lock==========

/*
 * Get current value of input then increase it.
 */
static __inline unsigned int
fetch_and_inc(volatile unsigned int *p)
{ 
    int v = 1;
    __asm __volatile (
    "   lock; xaddl   %0, %1 ;    "
    : "+r" (v),
      "=m" (*p)
    : "m" (*p));
 
    return (v);
}

void 
abql_lock_init(struct abql_lock_t *lock, uint nprocs)
{
    int i;
    for(i = 0; i < nprocs; i++) lock->has_lock[i].x = 0;
    lock->has_lock[0].x = 1;
    lock->queueLast = 0;
    lock->holderPlace = 0;
}

void
abql_lock_acquire(struct abql_lock_t *lock)
{
    int myPlace = fetch_and_inc(&lock->queueLast);
    while(lock->has_lock[myPlace % n].x == 0);
    lock->has_lock[myPlace % n].x = 0;
    lock->holderPlace = myPlace;
}

void
abql_lock_release(struct abql_lock_t *lock)
{
  int nxt = (lock->holderPlace + 1) % n;
  lock->has_lock[nxt].x = 1;
}
