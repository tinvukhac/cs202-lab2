#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "thread.h"

#define MAXTHREADS 128

static int numthreads = 0;
static int numpass = 0;

struct {
    struct spinlock_t lock;
    int thread[MAXTHREADS];
    int total;
}ttable;

struct {
    struct spinlock_t lock;
    int pass; 
    int holding_thread;
}frisbee;

void *pass_frisbee(void *arg)
{
    int i;
    int tid = *((int *) arg);

    spinlock_acquire(&ttable.lock);
    for(i = 0; i < MAXTHREADS; i++) {
        if(ttable.thread[i] == 0) {
            ttable.thread[i] = tid;
            break;
        }
    }
    ttable.total++;
    spinlock_release(&ttable.lock);

    for(;;) {
        spinlock_acquire(&ttable.lock);
        if(ttable.total == numthreads)
            goto start;
        spinlock_release(&ttable.lock);
    }

start:
    spinlock_release(&ttable.lock);

    for(;;) {
        spinlock_acquire(&frisbee.lock);

        if(frisbee.pass > numpass) {
            spinlock_release(&frisbee.lock);
            break;
        }
        
        if(frisbee.holding_thread == tid){
            spinlock_release(&frisbee.lock);
            sleep(5);
            continue;
        }
        
        if(frisbee.pass > 0)
            printf(1,"Pass number no: %d, Thread %d is passing the token to thread %d\n", frisbee.pass, frisbee.holding_thread, tid);
        frisbee.pass++;
        frisbee.holding_thread = tid;
        spinlock_release(&frisbee.lock);
    }

    exit();
}

int
main(int argc, char *argv[])
{
    if(argc != 3)
    {
        printf(2, "usage: frisbee <numberofthreads> <numberofpasses>\n");
        exit();
    }

    int i;

    numthreads = atoi(argv[1]);
    numpass = atoi(argv[2]);

    spinlock_init(&ttable.lock);
    ttable.total = 0;
    for(i = 0; i < MAXTHREADS; i++) {
        ttable.thread[i] = 0;
    }
    
    spinlock_init(&frisbee.lock);
    frisbee.pass = 0;
    frisbee.holding_thread = 0;
    
    for(i = 1; i <= numthreads; i++) {
        int *arg = malloc(sizeof(*arg));
        *arg = i;
        thread_create(pass_frisbee, arg);
    }

    for(i = 0; i < numthreads; i++) {
        thread_join();
    }

    printf(1, "Simulation of Frisbee game has finished, %d rounds were played in total!\n", numpass);  

    exit();
}
